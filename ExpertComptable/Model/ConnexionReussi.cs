﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ExpertComptable.Model
{
    internal class ConnexionReussi
    {
        public string strongConnexionReussieValue = "Bonjour, votre connexion est réussie";
        private WebDriverWait _wait;

        public ConnexionReussi(WebDriverWait _driverWait)
        {
            _wait = _driverWait;
        }

        public IWebElement strongConnexionReussi()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".Panel-maintext > strong:nth-child(2)")));
        }
    }
}