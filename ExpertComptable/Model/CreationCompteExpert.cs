﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;

/// <summary>
/// https://identification-rec2.experts-comptables.org/inscription_comptexpert.php?service=https%3A%2F%2Fidentification-rec.experts-comptables.org%2Fhome%2F
/// </summary>
namespace Model
{
    public class CreationCompteExpert
    {
        private WebDriverWait _wait;

        public CreationCompteExpert(WebDriverWait _driverWait)
        {
            _wait = _driverWait;
        }

        public IWebElement Nom()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("name")));
        }

        public IWebElement DateDeNaissance()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("date")));
        }

        public IWebElement BoutonCreationCompte()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("experpass_submit_button")));
        }

        public IWebElement ExpertComptableCliquezIci()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".inscription > p:nth-child(2) > a:nth-child(1)")));
        }

        public IWebElement Email()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("email")));
        }

        public IWebElement CompteUtilisateurCliquezIci()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".inscription > p:nth-child(1) > a:nth-child(2)")));
        }

        public IWebElement ChampChoixIdentifiant()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("username")));
        }

        public IWebElement ChampCivilite1()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("salutation")));
        }

        public IWebElement ChampCivilite2(string civilite)
        {
            if (civilite.Equals("Mme"))
            {
                return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#salutation > option:nth-child(2)")));
            }
            else
            {
                return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#salutation > option:nth-child(3)")));
            }
        }

        public IWebElement ChampNom()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("lastname")));
        }

        public IWebElement ChampPrenom()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("firstname")));
        }

        public IWebElement ChampTelephone()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("phone")));
        }

        public IWebElement ChampAdresse()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("adresse")));
        }

        public IWebElement ChampVille()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("ville")));
        }

        public IWebElement ChampCodePostal()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("cp")));
        }

        public IWebElement ChampAnneeDeNaissance1()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("birthyear")));
        }

        public IWebElement ChampAnneeDeNaissance2(string annee)
        {
            int choix = int.Parse(annee) - 1900 + 2;
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("p.line:nth-child(10) > span:nth-child(2) > select:nth-child(1) > option:nth-child(" + choix + ")")));
        }

        public IWebElement BoxRecevoirCommunications()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//input[@type='checkbox']")));
        }

        public ReadOnlyCollection<IWebElement> ResultatsNom()
        {
            return _wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("listNameResult")));
        }

        public IWebElement Identifiant()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("identifiant")));
        }

        public IWebElement SubmitIdentifiant()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("experpass_submit_button")));
        }

        public IWebElement AfficherPlusDeResultats()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#fm1 > div > p.moreResults")));
        }
    }
}