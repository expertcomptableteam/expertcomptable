﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Model
{
    public class RetrouverMesIdentifiants
    {
        private WebDriverWait _wait;

        public RetrouverMesIdentifiants(WebDriverWait _driverWait)
        {
            _wait = _driverWait;
        }

        public IWebElement Email()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("email")));
        }

        public IWebElement BoutonEnvoyer()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("experpass_submit_button")));
        }
    }
}