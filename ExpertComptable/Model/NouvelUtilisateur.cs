﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

///https://identification-rec2.experts-comptables.org/inscription.php?service=https://identification-rec.experts-comptables.org/home/
namespace Model
{
    public class NouvelUtilisateur
    {
        private WebDriverWait _wait;

        public NouvelUtilisateur(WebDriverWait _driverWait)
        {
            _wait = _driverWait;
        }

        public IWebElement ExpertComptable()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)")));
        }

        public IWebElement Q3ter()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)")));
        }

        public IWebElement Stagiaire()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(5) > a:nth-child(1)")));
        }

        public IWebElement Memorialiste()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(7) > a:nth-child(1)")));
        }

        public IWebElement ExpertComptableHonoraire()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)")));
        }

        public IWebElement MembreEce()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(11) > a:nth-child(1)")));
        }

        public IWebElement DiplomeNonInscrit()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(4) > a:nth-child(1)")));
        }

        public IWebElement PresidentDirecteurAGC()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(9) > a:nth-child(1)")));
        }

        public IWebElement Permanent()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(8) > a:nth-child(1)")));
        }

        public IWebElement LienMail()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".slice > p:nth-child(2) > a:nth-child(1)")));
        }

        public IWebElement Retour()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#main > p:nth-child(3) > a:nth-child(1)")));
        }

        public IWebElement AutreProfil()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(10) > a:nth-child(1)")));
        }

        public IWebElement FiliereCAFCAC()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".choix_type_compte > ul:nth-child(2) > li:nth-child(6) > a:nth-child(1)")));
        }

        public IWebElement h2NouvelUtilisateur()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#experpass > div > h2")));
        }
    }
}