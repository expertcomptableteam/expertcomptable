﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace ExpertComptable.Model
{
    public class Reinitialisation
    {
        WebDriverWait _wait;
        public Reinitialisation(IWebDriver _driver)
        {
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(20));
        }

        public IWebElement SaisieMdp()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("password")));
        }

        public IWebElement ConfirmerMdp()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("confirm_password")));
        }

        public IWebElement Valider()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("experpass_submit_button")));
        }

        public IWebElement ContinuerNavigation()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.LinkText("Continuer ma navigation")));
        }
    }
}
