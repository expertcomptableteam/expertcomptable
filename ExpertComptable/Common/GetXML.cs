﻿using System;
using System.Xml;

namespace Common
{
    static public class GetXML
    {
        static public string[] fichXml(string fileName, string element)
        {
            string[] tabTest = new string[10];
            int i = 0;
            string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string actualPath = path.Substring(0, path.LastIndexOf("bin"));
            string projectPath = new Uri(actualPath).LocalPath;
            string xmlPath = projectPath + "Src\\" + fileName;

            using (XmlReader reader = XmlReader.Create(xmlPath))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement() && i < tabTest.Length)
                    {
                        if (reader.Name.ToString() == element)
                        {
                            tabTest[i] = reader.ReadString();
                            i++;
                        }
                    }
                }
            }
            return tabTest;
        }
    }
}