@echo off
cd ../nunit-console - Chrome
start cmd /k nunit3-console.exe %~dp0ExpertComptable\bin\Chrome\ExpertComptable.dll  --where "class=~SuiteTest"
cd ../nunit-console - Firefox
start cmd /k nunit3-console.exe %~dp0ExpertComptable\bin\Firefox\ExpertComptable.dll  --where "class=~SuiteTest"
