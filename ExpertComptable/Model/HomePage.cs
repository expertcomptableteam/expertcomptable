﻿using Common;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

/// <summary>
/// https://identification-rec2.experts-comptables.org/cas/login?service=https://identification-rec.experts-comptables.org/home/
/// </summary>
namespace Model
{
    public class HomePage : SetUp
    {
        public string h2ConnectezVousValue = "CONNECTEZ-VOUS (INSTANCE DE RECETTE AGARIK)";
        public string authentificationValue = "S'authentifier";
        public string mdpOublieValue = "J'ai oublié mon mot de passe";
        public string nouvelUtilisateurValue = "NOUVEL UTILISATEUR";
        public string creeezCompteExpertValue = "Créez votre Comptexpert";
        public string assistanceValue = "ASSISTANCE";
        public string retrouvezIdsValue = "Retrouver mes identifiants";
        public string erreurConnexionsValue = "Mauvais identifiant / mot de passe.";

        private WebDriverWait _wait;

        public HomePage(WebDriverWait _driverWait)
        {
            _wait = _driverWait;
        }

        public IWebElement LogoOrdreDesExpert()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ordre_authentification")));
        }

        public IWebElement NomAppli()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".respresize")));
        }

        public IWebElement h2ConnectezVous()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("div.slice:nth-child(1) > h2:nth-child(1)")));
        }

        public IWebElement messageDerreur()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("status")));
        }

        ///<returns>retourne position du champs Identifiant de la homepage.</returns>
        public IWebElement Identifiant()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("username")));
        }

        ///<returns>retourne position du champs Mdp de la homepage.</returns>
        public IWebElement Mdp()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("password")));
        }

        ///<returns>retourne position du bouton s'authentifier de la homepage.</returns>
        public IWebElement Authentification()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("experpass_submit_button")));
        }

        public IWebElement JaiOublierMonMdp()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".form > ul:nth-child(7) > li:nth-child(1) > a:nth-child(1)")));
        }

        //CF si image = bonne ref??
        public IWebElement ConnectezVousSignExpert()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(".form > ul:nth-child(7) > li:nth-child(1) > a:nth-child(1)")));
        }

        //CF si image = bonne ref??
        public IWebElement ConnectezVousExpertPass()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("p.button:nth-child(2) > a:nth-child(1) > img:nth-child(2)")));
        }

        ///<returns>retourne position du bouton signExpert de la homepage.</returns>
        ///????
        public IWebElement SignExpert()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/div/div[2]/div[1]/div[2]/div/p[1]/a/img")));
        }

        ///<returns>retourne position du bouton Expertpass de la homepage.</returns>
        ///?????
        public IWebElement ExpertPass()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/div/div[2]/div[1]/div[2]/div/p[2]/a/img")));
        }

        public IWebElement NouvelUtilisateur()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#signexpert > h2:nth-child(1)")));
        }

        ///<returns>retourne position du bouton creez compte expert de la homepage.</returns>
        public IWebElement CreezCompteExpert()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#signexpert > div:nth-child(3) > p:nth-child(1) > a:nth-child(1)")));
        }

        public IWebElement Assistance()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("div.slice:nth-child(3) > h2:nth-child(1)")));
        }

        ///<returns>retourne position du bouton Retrouver mes identifiants de la homepage.</returns>
        public IWebElement RetrouverIdentifiant()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("div.slice:nth-child(3) > div:nth-child(2) > p:nth-child(1) > a:nth-child(1)")));
        }

        ///<returns>retourne position du text  de la homepage.</returns>
        public IWebElement TextVerif()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/div/div[2]/div[1]/h2")));
        }

        public IWebElement AideEnLigne()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("div.slice:nth-child(3) > strong:nth-child(4) > a:nth-child(1)")));
        }

        public IWebElement ContactezNous()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("#support-experpass")));
        }

        public IWebElement QrCode()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id=\"qrCodeId\"]/img[2]")));
        }
    }
}