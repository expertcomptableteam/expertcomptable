﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

/// <summary>
/// https://identification-rec2.experts-comptables.org/inscription_comptexpert.php?service=https%3A%2F%2Fidentification-rec2.experts-comptables.org%2Fhome%2F
/// </summary>
namespace ExpertComptable.Model
{
    internal class CompteActive
    {
        private WebDriverWait _wait;

        public CompteActive(WebDriverWait _driverWait)
        {
            _wait = _driverWait;
        }

        public IWebElement IdentifierDesMaintenant()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#experpass > div > div > div > ul > li:nth-child(1) > a")));
        }

        public IWebElement RetrouverMonMotDePass()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#experpass > div > div > div > ul > li:nth-child(3) > a")));
        }

        public IWebElement ContacterUnAdmin()
        {
            return _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#experpass > div > div > div > ul > li:nth-child(5) > a")));
        }
    }
}