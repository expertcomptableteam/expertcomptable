﻿using Common;
using ExpertComptable.Model;
using Model;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace Controller
{
    /// <summary>
    /// Tests class. Each methode is a test case.
    /// You can change the browser using the "TestFixture" note below.(Be sure to change the setup and the reportportal config file
    /// according to the browser)
    /// They are a lot of "Thread.sleep" directives that can be suppressed, they were added for the demo in front of the customer.
    /// Every variables are stored in .xml files and can be customed.
    /// </summary>
    [TestFixture(BrowserType.Firefox)]
    public class SuiteTest : SetUp
    {
        private HomePage homePage;
        private NouvelUtilisateur nouvelUtilisateur;
        private CreationCompteExpert creationCompteExpert;
        private RetrouverMesIdentifiants retrouverMesIdentifiants;
        private CompteActive compteActive;
        private Reinitialisation reinitialisation;

        public SuiteTest(BrowserType browser) : base(browser)
        {
            homePage = new HomePage(_driverWait);
            nouvelUtilisateur = new NouvelUtilisateur(_driverWait);
            creationCompteExpert = new CreationCompteExpert(_driverWait);
            retrouverMesIdentifiants = new RetrouverMesIdentifiants(_driverWait);
            compteActive = new CompteActive(_driverWait);
            reinitialisation = new Reinitialisation(_driverWait);
        }

        #region Step 1.1 Verification Page D'accueil

        [Test, Category("Ergonomique"), TestCase(TestName = "Verification Page D'accueil")]
        public void TestErgonomiqueAccueil()
        {
            #region Step 1.1

            Assert.IsTrue(homePage.LogoOrdreDesExpert().Displayed);
            Assert.IsTrue(homePage.NomAppli().Displayed);
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));
            Assert.IsTrue(homePage.Identifiant().Displayed);
            Assert.IsTrue(homePage.Identifiant().Displayed);
            Assert.IsTrue(homePage.Authentification().Text.Equals(homePage.authentificationValue));
            Assert.IsTrue(homePage.JaiOublierMonMdp().Text.Equals(homePage.mdpOublieValue));
            Assert.IsTrue(homePage.NouvelUtilisateur().Text.Equals(homePage.nouvelUtilisateurValue));
            Assert.IsTrue(homePage.CreezCompteExpert().Text.Equals(homePage.creeezCompteExpertValue));
            Assert.IsTrue(homePage.Assistance().Text.Equals(homePage.assistanceValue));
            Assert.IsTrue(homePage.RetrouverIdentifiant().Text.Equals(homePage.retrouvezIdsValue));

            #endregion Step 1.1
        }

        #endregion Step 1.1 Verification Page D'accueil

        #region Step 1-2 Creation du compte expert comptable

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte expert comptable")]
        public void CreationCompteExpertComptable()
        {
            string nom, prenom, date, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[0];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[0];
            date = GetXML.fichXml("JDD1.xml", "date")[0];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[0];

            #region Step 1.2

            homePage.CreezCompteExpert().Click();

            #endregion Step 1.2

            #region Step 1.3

            nouvelUtilisateur.ExpertComptable().Click();

            #endregion Step 1.3

            #region Step 1.4

            FillDateAndName(nom, prenom, date);

            #endregion Step 1.4

            #region Step 1.5

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 1.5

            Thread.Sleep(3000);

            #region Step 1.6

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 1.6

            #region Step 1.6

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);
            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 1.6

            #region Step 1.7

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 1.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[0];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[0];
            date = GetXML.fichXml("JDD.xml", "date")[0];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[0];

            #region Step 2.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 2.1

            #region Step 2.2

            nouvelUtilisateur.ExpertComptable().Click();

            #endregion Step 2.2

            #region Step 2.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 2.3

            #region Step 2.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 2.4

            Thread.Sleep(3000);

            #region Step 2.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 2.5

            #region Step 2.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();
            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.ExpertComptable().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();
            Thread.Sleep(3000);

            #endregion Step 2.6

            #region Step 2.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();
            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.ExpertComptable().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();
            Thread.Sleep(3000);

            #endregion Step 2.7

            #region Step 2.8

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);
            string Tab2 = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab2 = _driver.SwitchTo().Window(newTabHandle);
            var expectedTabH = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 2.8

            #region Step 2.9

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 2.9
        }

        #endregion Step 1-2 Creation du compte expert comptable

        #region Step 3-4 Creation du compte Q3Ter

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte Q3Ter")]
        public void CreationCompte83Ter()
        {
            string nom, prenom, date, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[1];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[1];
            date = GetXML.fichXml("JDD1.xml", "date")[1];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[1];

            #region Step 3.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 3.1

            #region Step 3.2

            nouvelUtilisateur.Q3ter().Click();

            #endregion Step 3.2

            #region Step 3.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 3.3

            #region Step 3.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 3.4

            #region Step 3.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 3.5

            #region Step 3.6

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 3.6

            #region Step 3.7

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 3.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[1];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[1];
            date = GetXML.fichXml("JDD.xml", "date")[1];
            date = GetXML.fichXml("JDD.xml", "mdp")[1];

            #region Step 4.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 4.1

            #region Step 4.2

            nouvelUtilisateur.Q3ter().Click();

            #endregion Step 4.2

            #region Step 4.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 4.3

            #region Step 4.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 4.4

            #region Step 4.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 4.5

            #region Step 4.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.Q3ter().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();

            #endregion Step 4.6

            #region Step 4.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.Q3ter().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 4.7

            #region Step 4.8

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab2 = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab2 = _driver.SwitchTo().Window(newTabHandle);
            var expectedTabH = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 4.8

            #region Step 4.9

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 4.9
        }

        #endregion Step 3-4 Creation du compte Q3Ter

        #region Step 5-6 Creation du compte expert stagiaire

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte expert stagiaire")]
        public void CreationCompteStagiaire()
        {
            string nom, prenom, date, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[2];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[2];
            date = GetXML.fichXml("JDD1.xml", "date")[2];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[2];

            #region Step 5.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 5.1

            #region Step 5.2

            nouvelUtilisateur.Stagiaire().Click();

            #endregion Step 5.2

            #region Step 5.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 5.3

            #region Step 5.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 5.4

            #region Step 5.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 5.5

            #region Step 5.6

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);
            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 5.6

            #region Step 5.7

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 5.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[2];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[2];
            date = GetXML.fichXml("JDD.xml", "date")[2];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[2];

            #region Step 6.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 6.1

            #region Step 6.2

            nouvelUtilisateur.Stagiaire().Click();

            #endregion Step 6.2

            #region Step 6.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 6.3

            #region Step 6.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 6.4

            #region Step 6.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 6.5

            #region Step 6.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();
            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.Stagiaire().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();

            #endregion Step 6.6

            #region Step 6.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();
            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.Stagiaire().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 6.7

            #region Step 6.8

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab2 = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab2 = _driver.SwitchTo().Window(newTabHandle);
            var expectedTabH = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 6.8

            #region Step 6.9

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 6.9
        }

        #endregion Step 5-6 Creation du compte expert stagiaire

        #region Step 7-8 Creation du compte expert memorialiste // pas d'accès au compte

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte expert memorialiste")]
        public void CreationCompteMemorialiste()
        {
            string nom, prenom, date, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[3];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[3];
            date = GetXML.fichXml("JDD1.xml", "date")[3];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[3];

            #region Step 7.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 7.1

            #region Step 7.2

            nouvelUtilisateur.Memorialiste().Click();

            #endregion Step 7.2

            #region Step 7.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 7.3

            #region Step 7.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 7.4

            #region Step 7.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 7.5

            #region Step 7.6

            Thread.Sleep(5000);

            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 7.6

            #region Step 7.7

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 7.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[3];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[3];
            date = GetXML.fichXml("JDD.xml", "date")[3];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[3];

            #region Step 8.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 8.1

            #region Step 8.2

            nouvelUtilisateur.Memorialiste().Click();

            #endregion Step 8.2

            #region Step 8.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 8.3

            #region Step 8.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 8.4

            #region Step 8.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 8.5

            #region Step 8.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();
            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.Memorialiste().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();

            #endregion Step 8.6

            #region Step 8.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();
            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.Memorialiste().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 8.7

            #region Step 8.8

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab2 = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab2 = _driver.SwitchTo().Window(newTabHandle);
            var expectedTabH = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 8.8

            #region Step 8.9

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 8.9
        }

        #endregion Step 7-8 Creation du compte expert memorialiste // pas d'accès au compte

        #region Step 9-10 Creation du compte expert comptable honoraire

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte expert comptable honoraire")]
        public void CreationCompteExpertComptableHonoraire()
        {
            string nom, prenom, date, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[4];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[4];
            date = GetXML.fichXml("JDD1.xml", "date")[4];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[4];

            #region Step 9.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 9.1

            #region Step 9.2

            nouvelUtilisateur.ExpertComptableHonoraire().Click();

            #endregion Step 9.2

            #region Step 9.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 9.3

            #region Step 9.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 9.4

            #region Step 9.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 9.5

            #region Step 9.6

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 9.6

            #region Step 9.7

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 9.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[4];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[4];
            date = GetXML.fichXml("JDD.xml", "date")[4];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[4];

            #region Step 10.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 10.1

            #region Step 10.2

            nouvelUtilisateur.ExpertComptableHonoraire().Click();

            #endregion Step 10.2

            #region Step 10.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 10.3

            #region Step 10.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 10.4

            #region Step 10.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 10.5

            #region Step 10.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.ExpertComptableHonoraire().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();

            #endregion Step 10.6

            #region Step 10.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.ExpertComptableHonoraire().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 10.7

            #region Step 10.8

            Thread.Sleep(5000);
            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab2 = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab2 = _driver.SwitchTo().Window(newTabHandle);
            var expectedTabH = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 10.8

            #region Step 10.9

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 10.9
        }

        #endregion Step 9-10 Creation du compte expert comptable honoraire

        #region Step 11-12 Creation du compte membre ece

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte membre ece")]
        public void CreationCompteMembreEce()
        {
            string nom, prenom, date, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[5];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[5];
            date = GetXML.fichXml("JDD1.xml", "date")[5];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[5];

            #region Step 11.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 11.1

            #region Step 11.2

            nouvelUtilisateur.MembreEce().Click();

            #endregion Step 11.2

            #region Step 11.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 11.3

            #region Step 11.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 11.4

            #region Step 11.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 11.5

            #region Step 11.6

            Thread.Sleep(5000);
            ValiderMailInit();

            #endregion Step 11.6

            #region Step 11.7

            Thread.Sleep(3000);

            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 11.7

            #region Step 11.8

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 11.8

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[5];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[5];
            date = GetXML.fichXml("JDD.xml", "date")[5];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[5];

            #region Step 12.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 12.1

            #region Step 12.2

            nouvelUtilisateur.MembreEce().Click();

            #endregion Step 12.2

            #region Step 12.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 12.3

            #region Step 12.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 12.4

            #region Step 12.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 12.5

            #region Step 12.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.MembreEce().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();

            #endregion Step 12.6

            #region Step 12.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.MembreEce().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 12.7

            #region Step 12.8

            Thread.Sleep(5000);
            ValiderMailInit();

            #endregion Step 12.8

            #region Step 12.9

            Thread.Sleep(3000);

            string Tab2 = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab2 = _driver.SwitchTo().Window(newTabHandle);
            var expectedTabH = GetXML.fichXml("TitleTab.xml", "dest")[0];

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 12.9
        }

        #endregion Step 11-12 Creation du compte membre ece

        #region step 13-14 Creation du compte membre diplome non inscrit

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte membre diplome non inscrit")]
        public void CreationCompteMembreDiplomeNonInscrit()
        {
            string nom, prenom, date, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[6];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[6];
            date = GetXML.fichXml("JDD1.xml", "date")[6];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[6];

            #region Step 13.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 13.1

            #region Step 13.2

            nouvelUtilisateur.DiplomeNonInscrit().Click();

            #endregion Step 13.2

            #region Step 13.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 13.3

            #region Step 13.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 13.4

            #region Step 13.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 13.5

            #region Step 13.6

            Thread.Sleep(5000);
            ValiderMailInit();

            #endregion Step 13.6

            #region Step 13.7

            Thread.Sleep(3000);

            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 13.7

            #region Step 13.8

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 13.8

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[6];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[6];
            date = GetXML.fichXml("JDD.xml", "date")[6];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[6];

            #region Step 14.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 14.1

            #region Step 14.2

            nouvelUtilisateur.DiplomeNonInscrit().Click();

            #endregion Step 14.2

            #region Step 14.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 14.3

            #region Step 14.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 14.4

            #region Step 14.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 14.5

            #region Step 14.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.DiplomeNonInscrit().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();

            #endregion Step 14.6

            #region Step 14.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.DiplomeNonInscrit().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 14.7

            #region Step 14.8

            Thread.Sleep(7000);
            ValiderMailInit();

            Thread.Sleep(3000);

            string Tab2 = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab2 = _driver.SwitchTo().Window(newTabHandle);
            var expectedTabhandle = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 14.8

            #region Step 14.9

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 14.9
        }

        #endregion step 13-14 Creation du compte membre diplome non inscrit

        #region Step 15 Creation du compte president

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte president")]
        public void CreationComptePresident()
        {
            string nom, prenom, date, mail, mdp;

            nom = GetXML.fichXml("JDD1.xml", "nom")[7];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[7];
            date = GetXML.fichXml("JDD1.xml", "date")[7];
            mail = GetXML.fichXml("JDD1.xml", "email")[7];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[7];

            #region Step 15.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 15.1

            #region Step 15.2

            nouvelUtilisateur.PresidentDirecteurAGC().Click();

            #endregion Step 15.2

            #region Step 15.3

            creationCompteExpert.ExpertComptableCliquezIci().Click();

            #endregion Step 15.3

            #region Step 15.4

            _driver.Navigate().Back();
            creationCompteExpert.Email().SendKeys(mail);
            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 15.4

            #region Step 15.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 15.5

            #region Step 15.6

            Thread.Sleep(5000);
            ValiderMailInit();

            #endregion Step 15.6

            #region Step 15.7

            Thread.Sleep(3000);

            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 15.7

            #region Step 15.8

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 15.8
        }

        #endregion Step 15 Creation du compte president

        #region Step 16 Creation du compte permanant

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte permanant")]
        public void CreationComptePermanent()
        {
            #region Step 16.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 16.1

            #region Step 16.2

            nouvelUtilisateur.Permanent().Click();

            #endregion Step 16.2

            #region Step 16.3

            nouvelUtilisateur.LienMail().Click();

            #endregion Step 16.3

            #region Step 16.4

            nouvelUtilisateur.Retour().Click();
            Assert.IsTrue(nouvelUtilisateur.h2NouvelUtilisateur().Text.Equals("NOUVEL UTILISATEUR"));

            #endregion Step 16.4

            System.Threading.Thread.Sleep(2000);
        }

        #endregion Step 16 Creation du compte permanant

        #region Step 17-18 Creation du compte autre profil

        [Test, Category("Fonctionnel"), TestCase(TestName = "Creation du compte autre profil")]
        public void CreationCompteAutreProfil()
        {
            string nom, prenom, date, email, ville, cp, telephone, civilite, adresse, mdp;
            nom = GetXML.fichXml("JDD1.xml", "nom")[8];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[8];
            date = GetXML.fichXml("JDD1.xml", "date")[8];
            email = GetXML.fichXml("JDD1.xml", "email")[8];
            ville = GetXML.fichXml("JDD1.xml", "ville")[8];
            cp = GetXML.fichXml("JDD1.xml", "cp")[8];
            telephone = GetXML.fichXml("JDD1.xml", "telephone")[8];
            civilite = GetXML.fichXml("JDD1.xml", "civilite")[8];
            adresse = GetXML.fichXml("JDD1.xml", "adresse")[8];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[8];

            #region Step 17.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 17.1

            #region Step 17.2

            nouvelUtilisateur.AutreProfil().Click();

            #endregion Step 17.2

            #region Step 17.3

            creationCompteExpert.CompteUtilisateurCliquezIci().Click();

            #endregion Step 17.3

            #region Step 17.4

            _driver.Navigate().Back();
            creationCompteExpert.Email().SendKeys(email);
            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 17.4

            #region step 17.5

            creationCompteExpert.ChampChoixIdentifiant().SendKeys(nom + prenom);
            creationCompteExpert.ChampCivilite1().Click();
            creationCompteExpert.ChampCivilite2(civilite).Click();
            creationCompteExpert.ChampNom().SendKeys(nom);
            creationCompteExpert.ChampPrenom().SendKeys(prenom);
            creationCompteExpert.ChampTelephone().SendKeys(telephone);
            creationCompteExpert.ChampAdresse().SendKeys(adresse);
            creationCompteExpert.ChampVille().SendKeys(ville);
            creationCompteExpert.ChampCodePostal().SendKeys(cp);
            creationCompteExpert.ChampAnneeDeNaissance1().Click();
            System.Threading.Thread.Sleep(1000);
            creationCompteExpert.ChampAnneeDeNaissance2(date).Click();
            creationCompteExpert.BoxRecevoirCommunications().SendKeys(Keys.Space);

            creationCompteExpert.BoutonCreationCompte().Click();
            System.Threading.Thread.Sleep(5000);

            #endregion step 17.5

            #region Step 18.6

            ValiderMailInit();

            #endregion Step 18.6

            #region Step 18.7

            Thread.Sleep(3000);

            string TabTitle = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTa = _driver.SwitchTo().Window(newTabHandle);
            var expectedNewTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 18.7

            #region Step 18.8

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 18.8

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[8];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[8];
            date = GetXML.fichXml("JDD.xml", "date")[8];
            email = GetXML.fichXml("JDD.xml", "email")[8];
            ville = GetXML.fichXml("JDD.xml", "ville")[8];
            cp = GetXML.fichXml("JDD.xml", "cp")[8];
            telephone = GetXML.fichXml("JDD.xml", "telephone")[8];
            civilite = GetXML.fichXml("JDD.xml", "civilite")[8];
            adresse = GetXML.fichXml("JDD.xml", "adresse")[8];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[8];

            #region Step 18.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 18.1

            #region Step 18.2

            nouvelUtilisateur.AutreProfil().Click();

            #endregion Step 18.2

            #region Step 18.3

            creationCompteExpert.CompteUtilisateurCliquezIci().Click();

            #endregion Step 18.3

            #region Step 18.4

            _driver.Navigate().Back();
            creationCompteExpert.Email().SendKeys(GetXML.fichXml("JDD1.xml", "email")[8]);
            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 18.4

            #region step 18.5

            creationCompteExpert.ChampChoixIdentifiant().SendKeys(nom + prenom);
            creationCompteExpert.ChampCivilite1().Click();
            creationCompteExpert.ChampCivilite2(civilite).Click();
            creationCompteExpert.ChampNom().SendKeys(nom);
            creationCompteExpert.ChampPrenom().SendKeys(prenom);
            creationCompteExpert.ChampTelephone().SendKeys(telephone);
            creationCompteExpert.ChampAdresse().SendKeys(adresse);
            creationCompteExpert.ChampVille().SendKeys(ville);
            creationCompteExpert.ChampCodePostal().SendKeys(cp);
            creationCompteExpert.ChampAnneeDeNaissance1().Click();
            System.Threading.Thread.Sleep(800);
            creationCompteExpert.ChampAnneeDeNaissance2(date).Click();
            creationCompteExpert.BoxRecevoirCommunications().SendKeys(Keys.Space);
            System.Threading.Thread.Sleep(5000);
            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion step 18.5

            #region Step 18.6

            ValiderMailInit();

            #endregion Step 18.6

            #region Step 18.7

            Thread.Sleep(3000);

            string Tab = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabH);
            var expectedTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            #endregion Step 18.7

            #region Step 18.8

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 18.8
        }

        #endregion Step 17-18 Creation du compte autre profil

        #region step 19-20 Creation du compte pour membres de la filiere CFAC

        [TestCase(TestName = "Creation du compte pour membres de la filiere CFAC")]
        public void CreationCompteFiliereCFAC()
        {
            string nom, prenom, date, mdp;
            nom = GetXML.fichXml("JDD1.xml", "nom")[9];
            prenom = GetXML.fichXml("JDD1.xml", "prenom")[9];
            date = GetXML.fichXml("JDD1.xml", "date")[9];
            mdp = GetXML.fichXml("JDD1.xml", "mdp")[9];

            #region Step 19.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 19.1

            #region Step 19.2

            nouvelUtilisateur.FiliereCAFCAC().Click();

            #endregion Step 19.2

            #region Step 19.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 19.3

            #region Step 19.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 19.4

            #region Step 19.5

            creationCompteExpert.Identifiant().SendKeys(nom + prenom);
            creationCompteExpert.SubmitIdentifiant().Click();

            #endregion Step 19.5

            #region Step 19.6

            Thread.Sleep(5000);
            ValiderMailInit();

            #endregion Step 19.6

            #region Step 19.7

            Thread.Sleep(3000);

            string originalTabTitle = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabHandle);
            var expectedNewTabTitle = GetXML.fichXml("TitleTab.xml", "dest")[0];

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 19.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            nom = GetXML.fichXml("JDD.xml", "nom")[9];
            prenom = GetXML.fichXml("JDD.xml", "prenom")[9];
            date = GetXML.fichXml("JDD.xml", "date")[9];
            mdp = GetXML.fichXml("JDD.xml", "mdp")[9];

            #region Step 20.1

            homePage.CreezCompteExpert().Click();

            #endregion Step 20.1

            #region Step 20.2

            nouvelUtilisateur.FiliereCAFCAC().Click();

            #endregion Step 20.2

            #region Step 20.3

            FillDateAndName(nom, prenom, date);

            #endregion Step 20.3

            #region Step 20.4

            creationCompteExpert.BoutonCreationCompte().Click();

            #endregion Step 20.4

            #region Step 20.5

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 20.5

            #region Step 20.6

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.FiliereCAFCAC().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.RetrouverMonMotDePass().Click();

            #endregion Step 20.6

            #region Step 20.7

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.CreezCompteExpert().Click();
            nouvelUtilisateur.FiliereCAFCAC().Click();
            FillDateAndName(nom, prenom, date);
            creationCompteExpert.BoutonCreationCompte().Click();
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 20.7

            #region Step 21.5

            ValiderMailInit();

            #endregion Step 21.5

            #region Step 21.6

            Thread.Sleep(3000);

            string TabTitle = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabH = _driver.WindowHandles.Last();
            var newTa = _driver.SwitchTo().Window(newTabH);
            var expectedNewTab = GetXML.fichXml("TitleTab.xml", "dest")[0];

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 21.6
        }

        #endregion step 19-20 Creation du compte pour membres de la filiere CFAC

        #region step 21 Retrouver ses identifiants

        [Test, Category("Fonctionnel"), TestCase(TestName = "Retrouver ses identifiants")]
        public void RetrouverSesIdentifiants()
        {
            string mail = GetXML.fichXml("JDD.xml", "email")[0];
            string mdp = GetXML.fichXml("JDD.xml", "mdp")[0];

            #region Step 21.1

            homePage.RetrouverIdentifiant().Click();

            #endregion Step 21.1

            #region Step 21.2

            retrouverMesIdentifiants.Email().SendKeys(mail);
            retrouverMesIdentifiants.BoutonEnvoyer().Click();

            #endregion Step 21.2

            #region Step 21.3

            compteActive.IdentifierDesMaintenant().Click();
            Assert.IsTrue(homePage.h2ConnectezVous().Text.Equals(homePage.h2ConnectezVousValue));

            #endregion Step 21.3

            #region Step 21.4

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            homePage.RetrouverIdentifiant().Click();
            retrouverMesIdentifiants.Email().SendKeys(mail);
            retrouverMesIdentifiants.BoutonEnvoyer().Click();
            Thread.Sleep(1000);
            compteActive.ContacterUnAdmin().Click();

            #endregion Step 21.4

            #region Step 21.5

            ValiderMailInit();

            #endregion Step 21.5

            #region Step 21.6

            Thread.Sleep(3000);

            string originalTabTitle = GetXML.fichXml("TitleTab.xml", "src")[0];

            Thread.Sleep(1000);
            string newTabHandle = _driver.WindowHandles.Last();
            var newTab = _driver.SwitchTo().Window(newTabHandle);
            var expectedNewTabTitle = GetXML.fichXml("TitleTab.xml", "dest")[0];

            reinitialisation.SaisieMdp().SendKeys(mdp);
            reinitialisation.ConfirmerMdp().SendKeys(mdp);
            reinitialisation.Valider().Click();

            #endregion Step 21.6

            #region Step 21.7

            reinitialisation.ContinuerNavigation().Click();

            #endregion Step 21.7
        }

        #endregion step 21 Retrouver ses identifiants

        #region step 22-23 Connexion

        [TestCase(TestName = "Connexion")]
        public void SeConnecterParam()
        {
            #region parametres

            int nbEssaies = int.Parse(GetXML.fichXml("Parametres Connexion.xml", "nbEssaies")[0]);
            int nbMinutesBlocage = int.Parse(GetXML.fichXml("Parametres Connexion.xml", "NbMinutesBlocage")[0]);
            string fakeMdp = GetXML.fichXml("Parametres Connexion.xml", "fakeMdp")[0];

            string idEnregistre = GetXML.fichXml("JDD.xml", "id")[0];
            string mdpEnregistre = GetXML.fichXml("JDD.xml", "mdp")[0];

            string idNonEnregistre = GetXML.fichXml("JDD1.xml", "nom")[0] + GetXML.fichXml("JDD1.xml", "prenom")[0];
            string mdpNonEnregistre = GetXML.fichXml("JDD1.xml", "mdp")[0];

            #endregion parametres

            #region Step 22.1

            Connexion(idEnregistre, mdpEnregistre);
            try
            {
                Assert.IsFalse(homePage.messageDerreur().Displayed);
            }
            catch (TimeoutException ex)
            {
            }

            #endregion Step 22.1

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            #region Step 23.1

            Connexion(idNonEnregistre, mdpNonEnregistre);
            try
            {
                Assert.IsTrue(homePage.messageDerreur().Displayed);
            }
            catch (TimeoutException ex)
            {
            }

            #endregion Step 23.1

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            #region Step 23.2

            Connexion(idEnregistre, fakeMdp);
            try
            {
                Assert.IsTrue(homePage.messageDerreur().Displayed);
            }
            catch (TimeoutException ex)
            {
            }

            #endregion Step 23.2

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
            _driver.Manage().Cookies.DeleteAllCookies();

            #region 23.3

            for (int i = 0; i < nbEssaies; i++)
            {
                try
                {
                    Connexion(idNonEnregistre, mdpEnregistre);
                    Assert.IsTrue(homePage.messageDerreur().Displayed);
                }
                catch (TimeoutException ex)
                {
                }
            }

            #endregion 23.3
        }

        #endregion step 22-23 Connexion

        #region step 24 Check QRCode

        [Category("Ergonomique"), TestCase(TestName = "Vérification du QrCode")]
        public void CheckQRCode()
        {
            #region Step 24.1

            homePage.ExpertPass().Click();

            #endregion Step 24.1

            System.Threading.Thread.Sleep(5000);
            try
            {
                Assert.IsTrue(homePage.QrCode().Displayed);
            }
            catch (UnhandledAlertException ex)
            {
                _driver.SwitchTo().Alert().Accept();
                Assert.IsTrue(false);
            }
            finally
            {
            }
        }

        #endregion step 24 Check QRCode

        #region Utility

        public void Connexion(string id, string mdp)
        {
            homePage.Identifiant().Clear();
            homePage.Identifiant().SendKeys(id);
            homePage.Mdp().Clear();
            homePage.Mdp().SendKeys(mdp);
            homePage.Authentification().Click();
        }

        /// <summary>
        /// This methode is mendatory to fill properly the first and last name while creating an account due to the ajax/javascript request.
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <param name="date"></param>
        public void FillDateAndName(string nom, string prenom, string date)
        {
            ReadOnlyCollection<IWebElement> tmp;
            creationCompteExpert.Nom().Clear();
            creationCompteExpert.Nom().SendKeys(nom);
            do
            {
                creationCompteExpert.Nom().SendKeys(" ");
                System.Threading.Thread.Sleep(2500);
                creationCompteExpert.Nom().SendKeys(Keys.Backspace);
                System.Threading.Thread.Sleep(2500);
                if (creationCompteExpert.AfficherPlusDeResultats().Displayed)
                {
                    creationCompteExpert.AfficherPlusDeResultats().Click();
                    System.Threading.Thread.Sleep(1500);
                }
            } while (creationCompteExpert.ResultatsNom()[0].Text.Contains(nom));

            tmp = creationCompteExpert.ResultatsNom();
            int i = 0;
            bool founded = false;
            while (!founded && i < tmp.Count)
            {
                if (tmp[i].Text.Contains(prenom.ToUpper()) || tmp[i].Text.Contains(prenom))
                {
                    tmp[i].Click();
                    founded = true;
                }
                i++;
            }
            Actions a = new Actions(_driver);
            a.MoveToElement(creationCompteExpert.DateDeNaissance()).Click();
            string tmpDate = date.Replace("/", string.Empty);
            Thread.Sleep(1000);
            a.MoveToElement(creationCompteExpert.DateDeNaissance()).SendKeys(tmpDate).Build().Perform();
        }

        public void ValiderMailReinit()
        {
            string email, mdp;

            email = GetXML.fichXml("Mail.xml", "email")[0];
            mdp = GetXML.fichXml("Mail.xml", "password")[0];

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "gmail")[0]); // login page

            _driver.FindElement(By.Id("identifierId")).SendKeys(email);
            _driver.FindElement(By.CssSelector(".CwaK9")).Click();
            Thread.Sleep(2000);
            _driver.FindElement(By.Name("password")).SendKeys(mdp);
            _driver.FindElement(By.ClassName("CwaK9")).Click();

            System.Threading.Thread.Sleep(5000); //pause for page to load

            _driver.FindElement(By.CssSelector("#\\3a 2e > td.yX.xYn")).Click();

            System.Threading.Thread.Sleep(2000); //pause for page to load

            _driver.FindElement(By.LinkText("réinitialiser mon mot de passe")).Click();
        }

        public void ValiderMailInit()
        {
            string email, mdp;

            email = GetXML.fichXml("Mail.xml", "email")[0];
            mdp = GetXML.fichXml("Mail.xml", "password")[0];

            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "gmail")[0]); // login page

            _driver.FindElement(By.Id("identifierId")).SendKeys(email);
            _driver.FindElement(By.CssSelector(".CwaK9")).Click();
            Thread.Sleep(1000);
            _driver.FindElement(By.Name("password")).SendKeys(mdp);
            _driver.FindElement(By.ClassName("CwaK9")).Click();

            System.Threading.Thread.Sleep(5000); //pause for page to load

            _driver.FindElement(By.CssSelector("#\\3a 2e > td.yX.xYn")).Click();

            System.Threading.Thread.Sleep(2000); //pause for page to load

            _driver.FindElement(By.LinkText("initialiser mon mot de passe")).Click();
        }

        #endregion Utility
    }
}