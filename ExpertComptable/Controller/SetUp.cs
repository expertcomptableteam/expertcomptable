﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using SeLoger.com.seloger.common.NunitTestingDigital;
using System;

namespace Common
{
    public enum BrowserType
    {
        Chrome,
        Firefox,
        IE,
        Edge,
        Opera,
        HeadLess
    }

    public class SetUp
    {
        protected CaseContext _caseContext = new CaseContext();
        protected IWebDriver _driver;
        protected WebDriverWait _driverWait;

        protected BrowserType _browserType;
        public string browserType = TestContext.Parameters.Get("Browser", "Chrome");

        public SetUp()
        {
        }

        public SetUp(BrowserType browser)
        {
            this._browserType = browser;
            _setWebDriver(browser);
            _driverWait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
        }

        [OneTimeSetUp]
        public void init()
        {
        }

        [SetUp]
        public void Setup()
        {
            _driver.Navigate().GoToUrl(GetXML.fichXml("Urls.xml", "value")[0]);
        }

        [TearDown]
        public void ApresLeTest()
        {
            _driver.Manage().Cookies.DeleteAllCookies();
        }

        [OneTimeTearDown]
        public void End()
        {
            _driver.Manage().Cookies.DeleteAllCookies();
            _driver.Close();
            _driver.Quit();
        }

        public void takeScreenshot(String filename)
        {
            ITakesScreenshot screenshotHandler = _driver as ITakesScreenshot;
            Screenshot screenshot = screenshotHandler.GetScreenshot();
            screenshot.SaveAsFile(filename + ".png", ScreenshotImageFormat.Png);
        }

        private void _setWebDriver(BrowserType browserType)
        {
            if (browserType == BrowserType.Chrome)
            {
                _driver = new ChromeDriver();
                _driver.Manage().Window.Maximize();
            }
            else if (browserType == BrowserType.Firefox)
            {
                _driver = new FirefoxDriver();
                _driver.Manage().Window.Maximize();
            }
            else if (browserType == BrowserType.IE)
            {
                _driver = new InternetExplorerDriver();
                _driver.Manage().Window.Maximize();
            }
            else if (browserType == BrowserType.Edge)
            {
                _driver = new EdgeDriver();
                _driver.Manage().Window.Maximize();
            }
        }
    }
}