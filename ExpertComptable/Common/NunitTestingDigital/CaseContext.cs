﻿using System;

namespace SeLoger.com.seloger.common.NunitTestingDigital
{
    public class CaseContext
    {
        public String _testID;
        public String _testName;
        public String _description;
        public String _category;

        public string TestID
        {
            get
            {
                return _testID;
            }

            set
            {
                _testID = value;
            }
        }

        public string TestName
        {
            get
            {
                return _testName;
            }

            set
            {
                _testName = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public string Category
        {
            get
            {
                return _category;
            }

            set
            {
                _category = value;
            }
        }
    }
}